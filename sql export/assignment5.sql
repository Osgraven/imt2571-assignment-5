-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2017 at 04:50 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `ClubId` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `CName` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `CCity` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `county`
--

CREATE TABLE `county` (
  `City` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `County` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE `season` (
  `UserName` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `FallYear` int(11) NOT NULL,
  `ClubId` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `TotalDistance` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skier`
--

CREATE TABLE `skier` (
  `UserName` varchar(60) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `FirstName` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `LastName` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `DateOfBirth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`ClubId`),
  ADD KEY `ClubCitytoCountiesCity` (`CCity`);

--
-- Indexes for table `county`
--
ALTER TABLE `county`
  ADD PRIMARY KEY (`City`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`UserName`,`FallYear`),
  ADD KEY `SeasonCIDtoClubCID` (`ClubId`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`UserName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `ClubCitytoCountiesCity` FOREIGN KEY (`CCity`) REFERENCES `county` (`City`);

--
-- Constraints for table `season`
--
ALTER TABLE `season`
  ADD CONSTRAINT `SeasonCIDtoClubCID` FOREIGN KEY (`ClubId`) REFERENCES `club` (`ClubId`),
  ADD CONSTRAINT `SeasonUNtoSkierUN` FOREIGN KEY (`UserName`) REFERENCES `skier` (`UserName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
