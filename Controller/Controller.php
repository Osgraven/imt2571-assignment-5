<?php

include_once("Model/Model.php");
include_once("View/View.php");
include_once("Model/XMLImporter.php");

class Controller{
	public $model;	//communicates with db
	public $importer;	//imports the xml
    public $skierList;	//lists of skiers, clubs and seasons.
    public $clubList;
    public $seasonList;
    
	public function __construct()
	{
        
		$this->importer = new XMLImporter();		//creates the object responsible for reading xml
        $skierList = $this->importer->readSkiers();	//imports the three lists of objects
        $clubList = $this->importer->readClubs();
		$seasonList = $this->importer->readSeasons();
		$this->model = new Model(null, $skierList, $seasonList, $clubList);	//creates the model, which exports to DB
		$this->model->addClubs();	//calls the export to db functions  (x3)
		$this->model->addSkiers();
		$this->model->addSeasons();
	}
	
	public function invoke()
	{        
		$view = new View();	//creates a view, could be used to show relevant progress, but CBA
        $view ->create();
	}
}
?>