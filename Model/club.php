<?php

//Holds the data related to a single club
class Club {
	public $ClubId;
	public $ClubName;
	public $ClubCity;
	public $County;

	public function __construct($ClubId, $ClubName, $ClubCity, $County)  
    {  
        $this->ClubId = $ClubId;
        $this->ClubName = $ClubName;
        $this->ClubCity = $ClubCity;
		$this->County = $County;
    } 
}

?>