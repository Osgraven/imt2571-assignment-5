<?php

//Unsure if i need these, as model have them imported.
include_once("skier.php");
include_once("season.php");
include_once("club.php");

class XMLImporter
{
    public $skierList;
    protected $domDoc;
    public function __construct()
    {
        $this->domDoc = new DOMDocument();
        $this->domDoc->load("SkierLogs.xml");
        
    }
		//no hocus pocus here, does pretty much as described
    public function readClubs()
    {
        $xpath = new DOMXpath($this->domDoc);		//creates dom xpath object
        $clubs = $xpath->query("//SkierLogs/Clubs/Club");	//makes the query
        $clubList = array();						//creates the array
        foreach($clubs as $club)					//iterates over the objects returned
        {
            $cID = $club->getAttribute('id');
			$cName = $club->getElementsByTagName('Name')->item(0)->textContent;
            $cCity = $club->getElementsByTagName('City')->item(0)->textContent;
            $cCounty = $club->getElementsByTagName('County')->item(0)->textContent;
            
            $clubList[] = new Club($cID, $cName, $cCity, $cCounty);
        }
        return $clubList;
    }
    
    public function readSkiers()
    {
        $xpath = new DOMXpath($this->domDoc);
        $skiers = $xpath->query("//SkierLogs/Skiers/Skier");
        $skierList = array();
        foreach($skiers as $skier)
        {
            $uN = $skier->getAttribute('userName');
            $fN = $skier->getElementsByTagName('FirstName')->item(0)->textContent;
            $lN = $skier->getElementsByTagName('LastName')->item(0)->textContent;
            $yOB = $skier->getElementsByTagName('YearOfBirth')->item(0)->nodeValue;
            
            $skierList[] = new Skier($uN, $fN, $lN, $yOB);
        }
        return $skierList;
    }
	
	public function readSeasons()	//this one is set apart by summing up the total distance, but still pretty much identical
    {

        $xpath = new DOMXpath($this->domDoc);
        $seasons = $this->domDoc->getElementsByTagName('Season');
        $seasonList = array();
        foreach($seasons as $season)
        {
            $fallYear = $season->getAttribute('fallYear');
			$clubs = $season->getElementsByTagName('Skiers');
			foreach($clubs as $club)
			{
				$clubId = $club->getAttribute('clubId');
				$skiers = $club->getElementsByTagName('Skier');
				foreach($skiers as $skier)
				{
					$skierUserName = $skier->getAttribute('userName');
					$entries = $skier->getElementsByTagName('Entry');
					$totalDistance = 0;
					foreach($entries as $entry)
					{
						$totalDistance += $entry->getElementsByTagName('Distance')->item(0)->nodeValue;
					}
					
					$seasonList[] = new Season($skierUserName, $fallYear, $clubId, $totalDistance);
					
				}
				
			}
		
        }
        return $seasonList;
    }
    
}

?>