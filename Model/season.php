<?php

//Holds the data related to a single season
class Season {
	public $UserName;
	public $FallYear;
	public $ClubId;
	public $TotalDistance;

	public function __construct($UserName, $FallYear, $ClubId, $TotalDistance)  
    {  
        $this->UserName = $UserName;
        $this->FallYear = $FallYear;
        $this->ClubId = $ClubId;
	    $this->TotalDistance = $TotalDistance;
    } 
}

?>