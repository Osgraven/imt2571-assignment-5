<?php

include_once("skier.php");
include_once("season.php");
include_once("club.php");

Class Model
{
    protected $db = null;
    protected $skierList;
	protected $seasonList;
	protected $clubList;
	
	
    public function __construct($db = null, $skiers, $seasons, $clubs)
    {
       if($db){
           $this->db = $db;			//creates the database object, no hocus pocus
       } else {
           $this->db = new PDO('mysql:host=localhost;dbname=assignment5;charset=utf8mb4', 'root', '');
       }
	   
	   $this->skierList = $skiers;		//lists of skiers, seasons and clubs
	   $this->seasonList = $seasons;
	   $this->clubList = $clubs;
        
    }
    
    public function addSkiers()
	{							//foreaches through skierList, and adds it to the DB through prepared statements
		
		foreach($this->skierList as $skier)
		{
			try
			{
				$tempSkier = $this->db->prepare('INSERT INTO skier (UserName, FirstName, LastName, DateOfBirth) VALUES (?,?,?,?)');
				$tempSkier->execute(array($skier->UserName, $skier->FirstName, $skier->LastName, $skier->DateOfBirth));
			} 
			catch(PDOException $ex)
			{
				echo "PDO Exception during addskier";
			}
		}
    }
    
    public function addSeasons()
	{				//same goes for these two.
        foreach($this->seasonList as $season)
		{
			try
			{
				$tempSeason = $this->db->prepare('INSERT INTO season (UserName, FallYear, ClubId, TotalDistance) VALUES (?,?,?,?)');
				$tempSeason->execute(array($season->UserName, $season->FallYear, $season->ClubId, $season->TotalDistance));
			} 
			catch(PDOException $ex)
			{
				echo "PDO Exception during addseason";
			}
		}
	}
    
    public function addClubs()
	{			//this one puts county into county aswell.
		foreach($this->clubList as $club)
		{
			try
			{
				$tempclub = $this->db->prepare('INSERT INTO club (ClubId, CName, CCity) VALUES (?,?,?)');
				$tempCounty = $this->db->prepare('INSERT INTO county(City, County) VALUES (?,?)');
				$tempCounty->execute(array($club->ClubCity, $club->County));
				$tempclub->execute(array($club->ClubId, $club->ClubName, $club->ClubCity));
			} 
			catch(PDOException $ex)
			{
            echo "PDO Exception during addclub";
			}
		}    
	}
}