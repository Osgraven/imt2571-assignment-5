<?php

//Holds the data related to a single skier
class Skier {
	public $UserName;
	public $FirstName;
	public $LastName;
	public $DateOfBirth;

	public function __construct($UserName, $FirstName, $LastName, $DateOfBirth)  
    {  
        $this->UserName = $UserName;
        $this->FirstName = $FirstName;
        $this->LastName = $LastName;
	    $this->DateOfBirth = $DateOfBirth;
    } 
}

?>